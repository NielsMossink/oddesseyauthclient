<?php

namespace OddesseySolutions\OAuth\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers;

use App\User;

class AuthController extends \App\Http\Controllers\Controller
{
    private $baseUrl;
    private $data;

    public function __construct()
    {
        $this->data = $this->getEnvironmentData();
        $this->baseUrl = config('oddesseysolutionsoauth.base_url');

    }

    public function logout(Request $request) {
        Auth::logout();
    }

    /* OAuth */
    public function startAuth(Request $request) {
        $query = http_build_query([
            'client_id' => $this->data['ODDESSEY_AUTH_CLIENT_ID'],
            'redirect_uri' => route('auth.callback'),
            'response_type' => 'code',
            'scope' => ''
        ]);

        return redirect("{$this->baseUrl}/oauth/authorize?$query");
    }

    public function callback(Request $request) {
        if($request->input('failed') == true) {
            echo $request->input('reason');
        } else {
            $response = (new \GuzzleHttp\Client)->post("{$this->baseUrl}/oauth/token", [
                'form_params' => [
                    'grant_type' => 'authorization_code',
                    'client_id' => $this->data['ODDESSEY_AUTH_CLIENT_ID'],
                    'client_secret' => $this->data['ODDESSEY_AUTH_CLIENT_SECRET'],
                    'redirect_uri' => route('auth.callback'),
                    'code' => $request->code,
                ]
            ]);

            $tokenData = json_decode((string) $response->getBody(), true);
            return $this->getUserData($tokenData['access_token']);
        }
    }

    private function getUserData($token) {
        $response = (new \GuzzleHttp\Client)->get("{$this->baseUrl}/user", [
            'headers' => [
                'Authorization' => 'Bearer ' . $token
            ]
        ]);

        $responseArray = json_decode((string) $response->getBody(), true);
        if(isset($responseArray['data'])) {
            $responseData = $responseArray['data'];
            if(isset($responseData['id'])) {
                $userModel = config('oddesseysolutionsoauth.user_model');
                // Look up the user
                $user = $userModel::where('oddessey_auth_id', $responseData['id'])->first();
                if(!isset($user)) {
                    // Check if a user with this email is already registered
                    $user = $userModel::where('email', $responseData['email'])->first();
                    if(isset($user)) {
                        $user->oddessey_auth_id = $responseData['id'];
                        $user->save();
                    } else {
                        $data = [
                            'email' => $responseData['email'],
                            'oddessey_auth_id' => $responseData['id'],
                            'password' => 'NO_LOCAL_AUTH'
                        ];

                        $nameStrategy = config('oddesseysolutionsoauth.new_user_name_strategy');
                        switch($nameStrategy) {
                            case 'name':
                                $data['name'] = $responseData['name'];
                                break;

                            case 'first_last':
                                $data['first_name'] = $responseData['name'];
                                $data['last_name'] = '';
                                break;

                            default:
                                echo "Invalid strategy '{$nameStrategy}";
                                exit;
                        }
                        
                        // Create a new user
                        $user = $userModel::create($data);
                    }
                }

                // Authorize the user
                Auth::login($user);

                return redirect($this->data['ODDESSEY_AUTH_REDIRECT_URL']);
            } else {
                echo 'ID not present';
            }
        }
    }

    private function getEnvironmentData() {
        $requiredKeys = [
            'ODDESSEY_AUTH_CLIENT_ID',
            'ODDESSEY_AUTH_CLIENT_SECRET',
            'ODDESSEY_AUTH_REDIRECT_URL',
        ];

        foreach($requiredKeys as $key) {
            $value = env($key);

            if(!isset($value) || empty($value)) {
                echo "Key '{$key}' not defined in environment. Define this in your applications env file before coninuing.";
                exit;
            }

            $data[$key] = $value;
        }

        return $data;
     }
}
