<?php 

return [
    'base_url' => 'http://auth.oddesseysolutions.nl',
    'new_user_name_strategy' => 'name', // Can be name or first_last
    'user_model' => 'App\User' 
];
